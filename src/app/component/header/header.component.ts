import { Component, Input, OnInit } from '@angular/core';
import { faHome, faNewspaper, faCartArrowDown, faCloudSunRain, faMapSigns } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() title !:string;

  /**
   * Parametre pour les icones
   */
  faHome = faHome;
  faNewspaper = faNewspaper;
  faCartArrowDown = faCartArrowDown;
  faCloudSunRain = faCloudSunRain;
  faCommunes = faMapSigns;

  constructor() { }

  ngOnInit(): void {
  }

}
