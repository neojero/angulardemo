import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.css']
})

export class NewArticleComponent implements OnInit {

  articleForm !: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private articleService : ArticleService,
    private router : Router,
    private notifService :NotificationService
  ) { }

  ngOnInit(): void {
    // appel de la methode d'initialisation du formulaire à l'initialisation du component
    this.initForm()
  }

  /**
   * Méthode de construction du formulaire
   */
  initForm() {
    // appel du formbuilder
    this.articleForm = this.formBuilder.group({
      // pour chaque champ initialisation + mise en place du validator
      title: ['', Validators.required],
      description: ['', Validators.required],
       url: ['', Validators.required],
      location: ['', Validators.required]
    });
  }

  /**
   * Methode de validation du formulaire
   */
  onSubmitForm() {
    // creation d'un nouvel ID
    let newId = this.articleService.newId();
    // recuperation des valeurs de mon formulaure sous forme de tableau
    const formValue = this.articleForm.value;
    // construction de mon objet avec affectation pour chaque propriété
    const newArticle = new Article(
      newId+1,
      formValue['title'],
      formValue['description'],
      new Date(),
      formValue['url'],
      0,
      formValue['location']
    );
    // ajour de mon item
    this.articleService.addArticle(newArticle);
    // notification de success
    this.notifService.showInfo('Nouvel article ajouté', 'New Article');
    // redirection vers la page articlelist
    this.router.navigate(['/articlelist']);
  }
}

