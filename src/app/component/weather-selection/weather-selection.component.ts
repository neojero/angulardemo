import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Weather } from 'src/app/models/weather';
import { WeatherService } from 'src/app/services/weather.service';
import { faCity } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-weather-selection',
  templateUrl: './weather-selection.component.html',
  styleUrls: ['./weather-selection.component.css']
})
export class WeatherSelectionComponent implements OnInit {

  @Input() weather !: Weather;
  @Output() getMeteo : EventEmitter<string> = new EventEmitter();

  faCity = faCity;

  search !: string;

  constructor () { }

  ngOnInit(): void { }

  getWeather() {
    return this.weather;
  }
}
