import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Departement } from 'src/app/models/departement.model';
import { faCircleNotch, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-departement-table',
  templateUrl: './departement-table.component.html',
  styles: [
  ]
})
export class DepartementTableComponent implements OnInit {

  /**
   * Parametres d'entrée
   */
  @Input() departements: Departement[] = [];
  @Input() departementsIsLoading!: boolean;
  @Input() departementsIsLoaded!: boolean;
  /**
   * Parametres de sortie
   * Mise en place d'evenement en lien avec Component departement
   */
  @Output() loadDepartements: EventEmitter<{}> = new EventEmitter();
  @Output() loadCommunes: EventEmitter<string> = new EventEmitter();

  /**
   * Parametre Icon
   */
  faCircleNotch = faCircleNotch;
  faSearch = faSearch;

  /**
   * Parametre interne
   */
  currentPage!: number;
  search!: string;
  departementsInProgress:boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.currentPage = 1;
    this.search = '';
  }

  /**
   * Methode de récuperation des departement en fonction de la recherche et découpage pour la pagination
   * @returns {Departement[]} retourne un tableau de departements
   */
  getDepartements(): Departement[] {
    return this.departements
        .filter(departement => departement.nom.toLowerCase().includes(this.search.toLowerCase()))
        .slice((this.currentPage - 1) * 10, this.currentPage * 10)
  }

  /**
   * Méthode de récuperation de nombre de département avec fitrage sur le nom de la recherche
   * @returns {number} retourne le nombre total de departement
   */
  getLength(): number {
    return this.departements
        .filter(departement => departement.nom.toLowerCase().includes(this.search.toLowerCase()))
        .length;
  }
}
