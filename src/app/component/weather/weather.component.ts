import { Component, OnInit } from '@angular/core';
import { Weather } from 'src/app/models/weather';
import { WeatherService } from 'src/app/services/weather.service';
import { faCloudSunRain } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  faCloudSunRain = faCloudSunRain;

  constructor(private weatherService :WeatherService) { }

  // objet vide
  weather: Weather = {
    city: "No City",
    conditions: "-",
    description: "-",
    temperature: 0,
    ressenti: 0,
    country: "-",
    icon: ""
  }

  ngOnInit(): void {
  }

  /**
   * Methode d'appel de récupération de la méteo
   * @param city la ville souhaité
   */
  onGetMeteo(city : string) {
    console.log("Update");
    this.weather = this.weatherService.getMeteo(city);
  }
}
