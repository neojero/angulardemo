import { Component, OnInit } from '@angular/core';
import { Commune } from 'src/app/models/commune.model';
import { Departement } from 'src/app/models/departement.model';
import { CommunesService } from 'src/app/services/communes.service';
import { DepartementsService } from 'src/app/services/departements.service';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-departements',
  templateUrl: './departements.component.html',
  styleUrls: ['./departements.component.css']
})
export class DepartementsComponent implements OnInit {

  /**
   * Parametres interne
   */
  departements: Departement[] = [];
  departementsIsLoading!: boolean;
  departementsIsLoaded!: boolean;

  communes: Commune[] = [];
  communesIsLoading!: boolean;
  communesIsLoaded!: boolean;

  communesForGraph: { name: string, value: number }[] = [];

  constructor(private departementService : DepartementsService, private communesService : CommunesService, private NotifService : NotificationService) { }

  ngOnInit(): void {

    this.departementsIsLoading = true;
    this.departementsIsLoaded = false;
    this.communesIsLoading = false;
    this.communesIsLoaded = false;
  }

  /**
   * Méthode lié à l'evenement pour rechercher les départements
   * avec gestion de notifications
   */
  onLoadDepartements(): void  {
    this.departementService.getDepartements().then((result) => {
    this.NotifService.showSuccess(`Liste des départements chargée`, 'Chargement ok');
    this.departementsIsLoaded = true;
    this.departementsIsLoading = false;
    this.departements = result as Departement[];
      }).catch( (err) => {
        if (err.error instanceof Error) {
          // we never seem to get here
          this.NotifService.showError(`Erreur ${err.error.message}`, 'Erreur Chargement:');
        } else {
          // no network connection, HTTP404, HTTP500, HTTP200 & invalid JSON
          this.NotifService.showError(`Retour code ${err.status} / ${err.name}`,'Erreur Chargement:');
        }
    });
  }

  /**
   * Méthode lié à l'evenement pour rechercher les communes
   * avec gestion de notification
   * @param codeDepartement
   */
  onLoadCommunes(codeDepartement: string): void {
    this.communesIsLoading = true;
    this.communesService.getCommunes(codeDepartement).then((result) => {

    this.communes = result as Commune[];
    // traitement pour le graphique
    this.communesForGraph = this.communes
      .filter(commune => commune.population > 5000)
      .map(commune => {
        return {
          name: commune.nom,
          value: commune.population
        };
      });
    this.communesIsLoaded = true;
    this.communesIsLoading = false;
    }).catch( (err) => {
      if (err.error instanceof Error) {
        // we never seem to get here
        this.NotifService.showError(`Erreur ${err.error.message}`, 'Erreur Chargement:');
      } else {
        // no network connection, HTTP404, HTTP500, HTTP200 & invalid JSON
        this.NotifService.showError(`Retour code ${err.status} / ${err.name}`,'Erreur Chargement:');
      }
    });
  }

  //
  // ANCIENNCE SERVICE AVANT MISE EN PLACE DE SERVICES
 //

  // onLoadDepartements(): void {
  //   this.httpClientService.get<Departement[]>(this.APIURL).subscribe(data => {
  //     this.NotifService.showSuccess(`Liste des départements chargée`, 'Chargement ok');
  //     this.departements = data;
  //     this.departementsIsLoaded = true;
  //     this.departementsIsLoading = false;
  //   },
  //   (err: HttpErrorResponse) => {
  //     if (err.error instanceof Error) {
  //       // we never seem to get here
  //       this.NotifService.showError(`Erreur ${err.error.message}`, 'Erreur Chargement:');
  //     } else {
  //       // no network connection, HTTP404, HTTP500, HTTP200 & invalid JSON
  //       this.NotifService.showError(`Retour code ${err.status} / ${err.name}`,'Erreur Chargement:');
  //     }
  //   });
  // }

  // onLoadCommunes(codeDepartement: string): void {
  //   this.communesIsLoading = true;
  //   this.httpClientService.get<Commune[]>(`https://geo.api.gouv.fr/departements/${codeDepartement}/communes`).subscribe(data => {
  //       this.NotifService.showSuccess(`Liste des communes du ${codeDepartement} chargée`, 'Chargement ok');
  //       this.communes = data;
  //       this.communesForGraph = data
  //           .filter(commune => commune.population > 8000)
  //           .map(commune => {
  //               return {
  //                   name: commune.nom,
  //                   value: commune.population
  //               };
  //           });
  //       this.communesIsLoaded = true;
  //       this.communesIsLoading = false;
  // },
  //   (err: HttpErrorResponse) => {
  //     if (err.error instanceof Error) {
  //       // we never seem to get here
  //       this.NotifService.showError(`Erreur ${err.error.message}`, 'Erreur Chargement');
  //     } else {
  //       // no network connection, HTTP404, HTTP500, HTTP200 & invalid JSON
  //       this.NotifService.showError(`Retour code ${err.status} / ${err.name}`,'Erreur Chargement');
  //     }
  //   });
}
