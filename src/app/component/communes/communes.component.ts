import { Component, Input, OnInit } from '@angular/core';
import { Commune } from 'src/app/models/commune.model';

@Component({
  selector: 'app-communes',
  templateUrl: './communes.component.html',
  styles: [
  ]
})
export class CommunesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Paramétre d'entrées
  */
  @Input() communes: Commune[] = [];
  @Input() communesIsLoading: boolean = false;
  @Input() communesIsLoaded: boolean = false;

  /**
   * Parametre interne au component
   */
  currentPage: number = 1;
  search: string = '';

  /**
   * Méthode de calcul du nombre de communes avec filtrage du nom
   * @returns {number} le nombre de communes
   */
  getLength(): number {
      return this.communes
          .filter(commune => commune.nom.toLowerCase().includes(this.search.toLowerCase()))
          .length;
  }

  /**
   * Méthode d'obtention de la liste des communes avec filtrage sur le nom
   * @returns {Commune[]} la liste des communes
   */
  getCommunes(): Commune[] {
      return this.communes
          .filter(commune => commune.nom.toLowerCase().includes(this.search.toLowerCase()))
          .slice((this.currentPage - 1) * 10, this.currentPage * 10);
  }

}
