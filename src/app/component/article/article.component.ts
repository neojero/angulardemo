import { Component, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { faMedal, faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { Article } from 'src/app/models/article';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() myArticle !: Article;

  faMedal = faMedal;
  faThumbsUp = faThumbsUp;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onAddSnap() {
    this.myArticle.snaps++;
  }

  onViewArticle() {
    this.router.navigateByUrl(`article/${this.myArticle.id}`);
}

}
