import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // pour la simulation de l'authentification
  authStatus !: boolean;

  constructor(
    private authService :AuthService,
    private router : Router,
    private notif : NotificationService
    ) {}

  ngOnInit(): void {
  }

  // Plus utilisé
  // onContinue() {
  //   this.router.navigateByUrl('articlelist');
  // }

  /**
   * Méthode pour simuler une authentification
   */
  onSignIn() {
    this.authService.signIn().then(
      () => {
        console.log('Sign in successful!');
        this.authStatus = this.authService.isAuth;
        // notification de connexion
        this.notif.showSuccess('Connexion réussie', 'Authentification');
        // redirection vers la page articlelist
        this.router.navigate(['articlelist']);
      }
    );
  }

  /**
   * Méthode pour simuler la déconnexion
   */
  onSignOut() {
    this.authService.signOut();
    this.authStatus = this.authService.isAuth;
    // notification de deconnexion
    this.notif.showWarming('Déconnexion réussie', 'Authentification');
    // redirection vers la page Home
    this.router.navigate(['home']);
  }

  /**
   * Méthode de contrôle de l'etat de l'authentification
   * @returns retourne l'etat de l'authentification
   */
  getAuthStatus() {
    return this.authService.isAuth;
  }

}
