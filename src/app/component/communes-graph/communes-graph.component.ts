import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-communes-graph',
  templateUrl: './communes-graph.component.html',
  styles: [
  ]
})
export class CommunesGraphComponent implements OnInit {

  /**
   * Parametres d'entrée
   */
  @Input() communesForGraph: { name: string, value: number }[] = [];
  @Input() communesIsLoading!: boolean;
  @Input() communesIsLoaded!: boolean;

  /** Parametre et option pour le graph */
  view!: [number, number];
  // options
  xAxisLabel !: string;
  legendTitle !: string;
  yAxisLabel !: string;
  legend !: boolean;
  showXAxisLabel !: boolean;
  showYAxisLabel !: boolean;
  xAxis !: boolean;
  yAxis !: boolean;
  gradient !: boolean;

  view2 !: [number, number];
  legend2 !: boolean;
  legendPosition !:string;

  constructor() { }

  ngOnInit(): void {

    /**
     * Initialisation des parametres et options
     */
    this.communesIsLoading = false;
    this.communesIsLoaded = false;
    this.view = [1000, 400];
    this.xAxisLabel ='Villes';
    this.legendTitle='Population par ville';
    this.yAxisLabel='Population';
    this.legend=true;
    this.showXAxisLabel=true;
    this.showYAxisLabel=true;
    this.xAxis=true;
    this.yAxis=true;
    this.gradient=false;

    this.view2 = [500, 400];
    this.legendPosition = 'below';

  }

}
