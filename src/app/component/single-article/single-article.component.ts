import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {

  title: string = 'title';
  description : string = 'description';

  constructor(private articleService : ArticleService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // recuperation de l'id au travers du snapshot qui est un instantané
    // + correspond à cast car la valeur récuperer est en string
    const snapId = +this.route.snapshot.params['id'];
    this.title = this.articleService?.getArticleById(snapId)?.title ?? 'undefined';
    this.description = this.articleService?.getArticleById(snapId)?.description ?? 'undefined';
  }

}
