import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ArticleListComponent } from "./component/article-list/article-list.component";
import { DepartementsComponent } from "./component/departements/departements.component";
import { Error404Component } from "./component/error404/error404.component";
import { HomeComponent } from "./component/home/home.component";
import { NewArticleComponent } from "./component/new-article/new-article.component";
import { SingleArticleComponent } from "./component/single-article/single-article.component";
import { WeatherComponent } from "./component/weather/weather.component";
import { AuthGuardService } from "./services/auth-guard.service";

// définition de nos routes + canActivate sur les routes à protéger
// ** = route wildcard
const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent },
  { path: 'articlelist', canActivate: [AuthGuardService], component: ArticleListComponent },
  { path: 'article/:id', canActivate: [AuthGuardService], component: SingleArticleComponent },
  { path: 'new', canActivate: [AuthGuardService], component: NewArticleComponent },
  { path: 'meteo', component: WeatherComponent},
  { path: 'departements', canActivate: [AuthGuardService], component: DepartementsComponent },
  { path: 'not-found', component: Error404Component},
  { path: '**', component: Error404Component}
];

@NgModule( {
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
