import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ArticleComponent } from './component/article/article.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ArticleListComponent } from './component/article-list/article-list.component';
import { HeaderComponent } from './component/header/header.component';
import { ArticleService } from './services/article.service';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './component/home/home.component';
import { SingleArticleComponent } from './component/single-article/single-article.component';
import { AuthService } from './services/auth.service';


import { FooterComponent } from './component/footer/footer.component';
import { Error404Component } from './component/error404/error404.component';
import { NewArticleComponent } from './component/new-article/new-article.component';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationService } from './services/notification.service';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService } from './services/auth-guard.service';
import { WeatherComponent } from './component/weather/weather.component';
import { WeatherService } from './services/weather.service';
import { WeatherSelectionComponent } from './component/weather-selection/weather-selection.component';

import { HttpClientModule } from '@angular/common/http';
import { CommunesComponent } from './component/communes/communes.component';
import { CommunesGraphComponent } from './component/communes-graph/communes-graph.component';
import { NgxChartsModule }from '@swimlane/ngx-charts';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DepartementsComponent } from './component/departements/departements.component';
import { DepartementTableComponent } from './component/departement-table/departement-table.component';
import { DepartementsService } from './services/departements.service';
import { CommunesService } from './services/communes.service';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ArticleListComponent,
    HeaderComponent,
    HomeComponent,
    SingleArticleComponent,
    FooterComponent,
    Error404Component,
    NewArticleComponent,
    WeatherComponent,
    WeatherSelectionComponent,
    CommunesComponent,
    DepartementsComponent,
    DepartementTableComponent,
    CommunesGraphComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PaginationModule,
    NgxChartsModule
  ],
  providers: [
    ArticleService,
    AuthService,
    NotificationService,
    AuthGuardService,
    WeatherService,
    DepartementsService,
    CommunesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
