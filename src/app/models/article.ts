export class Article {

  id: number;
  title: string;
  description: string;
  createDate: Date;
  snaps: number;
  imageUrl: string;

  //mise en place d'une propriétés optionnelle
  location !: string;

  constructor(pId: number, pTitle: string, pDescription: string, pCreateDate: Date, pImageUrl: string, pSnaps: number, pLocation ?:string) {
    this.id = pId;
    this.title = pTitle;
    this.description = pDescription;
    this.createDate = pCreateDate;
    this.imageUrl = pImageUrl;
    this.snaps = pSnaps;
    this.location = pLocation!;
  }

  // raccourcie TypeScript
  // constructor(public title: string,
  //               public description: string,
  //               public imageUrl: string,
  //               public createdDate: Date,
  //               public snaps: number) {
  //   }

}
