import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Commune } from '../models/commune.model';

@Injectable({
  providedIn: 'root'
})
export class CommunesService {

  constructor(private httpClientService: HttpClient) { }

  /**
   * Methode de récupération des données venant de l'API
   * @param codeDepartement le code du département recherché
   * @returns {Promise} retourne une promesse
   */
  getCommunes(codeDepartement: string) {
    let promise = new Promise ((resolve, reject) => {
      this.httpClientService
        .get<Commune[]>(`${environment.APIURLGOUV}${codeDepartement}/communes`)
        .toPromise()
        .then(
          data => {
            // sucess
            resolve(data);
          },
          err => {
            // erreur
            reject(err);
          }
        );
    });
    return promise;
 }
}
