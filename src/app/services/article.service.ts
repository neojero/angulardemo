import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Article } from '../models/article';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
/**
 * Service de gestion des articles
 */
export class ArticleService {

  /**
   * Ma liste d'articles
   */
  myArticles : Article[] = [
    {
      id: 1,
      title: 'une peluche perdue dans la nature',
      description: 'Un enfant recherche sa peluche',
      imageUrl: 'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg',
      createDate: new Date(),
      snaps: 0,
      location: 'Paris'
    },
    {
      id: 2,
      title: 'Three Rock Mountain',
      description: 'Un endroit magnifique pour les randonnées.',
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Three_Rock_Mountain_Southern_Tor.jpg/2880px-Three_Rock_Mountain_Southern_Tor.jpg',
      createDate: new Date(),
      snaps: 6,
      location: 'la montagne'
    },
    {
      id: 3,
      title: 'Un bon repas',
      description: 'Mmmh que c\'est bon !',
      imageUrl: 'https://wtop.com/wp-content/uploads/2020/06/HEALTHYFRESH.jpg',
      createDate: new Date(),
      snaps: 0,
      location: ""
    }
  ];

  constructor(private httpClient : HttpClient, private notifService : NotificationService) { }

  /**
   * Méthode de récuperation d'un article par son id
   * @param id number l'id de l'article recherché
   * @returns
   */
  getArticleById(id: number) {
    const article = this.myArticles.find(
      (s) => {
        return s.id === id;
      }
    );
    return article;
  }

  /**
   * Methode de comptage des articles
   * @returns number nombre d'elements
   */
  newId() : number {
    return this.myArticles.length;
  }

  /**
   * Méthode d'ajout d'un élement dans le tableau des articles
   * @param item Article l'article à ajouter
   */
  addArticle(item :Article) {
    this.myArticles.push(item);
  }

  /**
   * Méthode pour sauvegarder des données vers Firebase
   */
  saveToFireBase() {
    // envoi des articles à Firebase.
    // la méthode post écrit les données mais ne les écrasent pas ce qui fait doublon
    // priviliégés put alors à la place de post
    // les méthodes put et post prennent les mêmes arguments
    this.httpClient
      .put(environment.urlFirebase + 'articles.json', this.myArticles)
      .subscribe(
        () => {
          this.notifService.showSuccess("Enregistrement effectué", "Firebase connect");
        },
        (error) => {
          this.notifService.showError("Enregistrement non effectué", "Firebase connect");
        }
      );
  }

  /**
   * Méthode pour charger des données depuis Firebase
   */
  getFromFireBase() {
    this.httpClient
      .get<any[]>(environment.urlFirebase + 'articles.json')
      .subscribe(
        (response) => {
          this.myArticles = response;
          this.notifService.showSuccess("Récupération effectuée", "Firebase connect");
        },
        (error) => {
          this.notifService.showError("Récupération non effectuée", "Firebase connect");
        }
      );
  }
}
