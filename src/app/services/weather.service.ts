import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NotificationService } from './notification.service';
import { Weather } from '../models/weather';

@Injectable({
  providedIn: 'root'
})

/**
 * Service de gestion de l'appel à l'API Méteo Openweather
 */
export class WeatherService {

  /**
   * Objet Weather par défaut
   */
  myWeather : Weather = {
    city: "No City",
    conditions: "-",
    description: "-",
    temperature: 0,
    ressenti: 0,
    country: "",
    icon: ""
  }

  constructor(private httpClient : HttpClient, private notifService : NotificationService ) { }

  /**
   *
   * @param name Méthode d'obtention des informations méteo retournant un objet Weather contenant les informations
   * @returns Weather
   */
  getMeteo(name : string) : Weather {
    if (name) {
      //console.log(environment.apiurl + 'weather?q=' + name + ',54000,fr&lang=fr&APPID=' + environment.apikey + '&units=metric');
      // appel à l'API en construisant la requête avec les informations puis souscription
      this.httpClient.get(environment.apiurl + 'weather?q=' + name + '&lang=fr&APPID=' + environment.apikey + '&units=metric').subscribe( (response) => {
        // affichage message de réussite
        this.notifService.showSuccess(`Chargement ok`, 'Service Méteo');
        // construction de mon objet weather
        this.myWeather['city'] = name;
        this.myWeather['conditions'] = JSON.parse(JSON.stringify(response)).weather[0]['main'];
        this.myWeather['description'] = JSON.parse(JSON.stringify(response)).weather[0]['description'];
        this.myWeather['temperature'] = JSON.parse(JSON.stringify(response)).main.temp;
        this.myWeather['ressenti'] = JSON.parse(JSON.stringify(response)).main.feels_like;
        this.myWeather['country'] = JSON.parse(JSON.stringify(response)).sys.country;
        this.myWeather['icon'] = this.getIcon(JSON.parse(JSON.stringify(response)).weather[0]['icon']);
        return this.myWeather;
      },
      // gestion des cas d'erreurs avec affichage de notifications
      (err : HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // we never seem to get here
          this.notifService.showError(`Erreur ${err.error.message}`, 'Service Méteo');
        } else {
          // no network connection, HTTP404, HTTP500 & invalid JSON
          this.notifService.showError(`Retour code ${err.status} / ${err.name}`,'Service Méteo');
        }
      });
    }
    // return un objet par défaut si aucune saisie + notification
    if (!name) {
      this.notifService.showWarming(`Merci de renseigner une ville`, 'Service Méteo');
    }
    return this.myWeather;
  }

  /**
   * Méthode de récupération de l'image de l'icone météo
   * @param icon string Identifiant de l'icone
   * @returns
   */
  private getIcon(icon: string) {
    return 'http://openweathermap.org/img/w/' + icon + ".png";
  }
}
