import { Injectable } from '@angular/core';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})

/**
 * Service de gestion de l'authentification
 * Principe : Simuler une authentification dans le but de l'exercice
 */
export class AuthService {

  constructor() { }

  /**
   * boolean simulant l'authentification
   */
  isAuth = false;

  /**
   * Methode de connexion
   * Rend true aprés 2 secondes
   * @returns boolean
   */
  signIn() {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            this.isAuth = true;
            resolve(true);
          }, 2000
        );
      }
    );
  }

  /**
   * Methode de déconnexion
   */
  signOut() {
    this.isAuth = false;
  }
}
