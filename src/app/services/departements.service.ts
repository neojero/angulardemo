import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Departement } from '../models/departement.model';

@Injectable({
  providedIn: 'root'
})
export class DepartementsService {

  constructor(private httpClientService: HttpClient) { }


  /**
   * Methode de récupération des données venant de l'API
   * @returns {Promise}
   */
  getDepartements() {
    let promise = new Promise ((resolve, reject) => {
      this.httpClientService
        .get<Departement[]>(environment.APIURLGOUV)
        .toPromise()
        .then(
          data => {
            // success
            resolve(data);
          },
          err => {
            // erreur
            reject(err);
          }
        );
    });
    return promise;
  }
}
