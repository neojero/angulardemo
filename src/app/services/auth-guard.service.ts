import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})

/**
 * Service de gardien des routes
 */
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  /**
   * Méthode de contrôle Parfeu
   * @param route
   * @param state
   * @returns Promise | Observable | boolean
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean>| boolean {
    if(this.authService.isAuth) {
      return true;
    } else {
      this.router.navigate(['/home']);
      return false;
    }
  }
}
