import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

/**
 * Service de gestion des notifications Toastr
 */
export class NotificationService {

  /**
   * Contructeur appelant le service Toastr
   * @param toastService
   */
  constructor(private toastService : ToastrService) { }

  /**
   * Méthode d'affichage sur un sucess
   * @param message string
   * @param title string
   */
  showSuccess(message: string, title: string) {
    this.toastService.success(message, title);
  }

    /**
   * Méthode d'affichage sur une erreur
   * @param message string
   * @param title string
   */
  showError(message:string, title: string) {
    this.toastService.error(message, title);
  }

    /**
   * Méthode d'affichage sur une info
   * @param message string
   * @param title string
   */
  showInfo(message:string, title: string) {
    this.toastService.info(message, title);
  }

    /**
   * Méthode d'affichage sur un warming
   * @param message string
   * @param title string
   */
  showWarming(message:string, title: string) {
    this.toastService.warning(message, title);
  }
}
